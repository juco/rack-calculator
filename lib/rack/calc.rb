module Rack
  class Calc

    MAP = {
      plus: '+',
      minus: '-'
    }

    def initialize(path)
      @path = path
    end

    def calculate
      parts = split(@path)
      validate_parts!(parts)

      acc = parts.shift.to_i
      until parts.empty? do
        operator, number = parts.shift(2)
        validate_couple!(operator, number)

        acc = acc.send(MAP[operator.to_sym], number.to_i)
      end

      acc
    end

    private

      def validate_parts!(parts)
        if parts.length % 2 == 0
          raise InvalidPathError.new('The format must follow /1/plus/2 ...')
        end
        unless valid_number? parts.first
          raise InvalidPathError.new('The first part of the path should be a number')
        end
      end

      def validate_couple!(operator, number)
        unless valid_operator? operator
          valid_operators = MAP.keys.map(&:to_s).join(', ')
          raise InvalidPathError.new("Supported operators are: #{valid_operators}")
        end
        unless valid_number? number
          raise InvalidPathError.new('Numbers must be numeric')
        end
      end

      def valid_operator?(operator)
        MAP.keys.include? operator.to_sym
      end

      def valid_number?(number)
        (number =~ /\d+/) == 0
      end

      def split(path)
        path.split('/').reject { |item| item.empty? }
      end

      class InvalidPathError < StandardError; end;
  end
end
