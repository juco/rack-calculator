require_relative 'calc'

module Rack
  class Calculator
    def self.call(env)
      calculator = Rack::Calc.new(env['PATH_INFO'])
      response = calculator.calculate.to_s
      ['200', {'Content-Type' => 'text/html'}, [response]]
    rescue StandardError => e
      ['400', {'Content-Type' => 'text/html'}, [e.message]]
    end
  end
end
