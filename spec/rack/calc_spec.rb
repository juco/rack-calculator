describe 'Calc class' do

  it 'should calculate given the path /1/plus/3/minus/2/plus/5' do
    calc = Rack::Calc.new('/1/plus/3/minus/2/plus/5')
    expect(calc.calculate).to eq 7
  end

  it 'should calculate correctly given /30/minus/20' do
    calc = Rack::Calc.new('/30/minus/20')
    expect(calc.calculate).to eq 10
  end

  it 'should raise an error when the path shape is invalid' do
    expect { Rack::Calc.new('/4/plus').calculate }.to raise_error(
      Rack::Calc::InvalidPathError, /The format must follow/)
  end

  it 'should raise an erorr when the first path param is not numeric' do
    calc = Rack::Calc.new('/foo/plus/20')
    expect { calc.calculate }.to raise_error Rack::Calc::InvalidPathError, /first part/
  end

  it 'should raise an InvalidPathError for incorrect numbers' do
    calc = Rack::Calc.new('/20/plus/foo/minus/20')
    expect { calc.calculate }.to raise_error Rack::Calc::InvalidPathError, /Numbers must/
  end

end
