require "spec_helper"

describe Rack::Calculator do
  include Rack::Test::Methods
  let(:app) { described_class }

  it 'should return a 200 status code' do
    expect(get('/1/plus/3').status).to be 200
  end

  it 'should return 7 for the given sum' do
    req = get('/1/plus/3/minus/2/plus/5')
    expect(req.status).to be 200
    expect(req.body).to eq '7'
  end

  it 'should return an error for invalid inputs' do
    expect(get('/1/plus').status).to be 400
    expect(get('/1/foo/3').status).to be 400
    expect(get('/2/plus/xx').status).to be 400
  end
end
